console.log(`Hello World`);

function solveNumbers(){
	let num1 = parseInt(prompt(`Enter first number`));
	console.log(num1);

	let num2 = parseInt(prompt(`Enter second number`));
	console.log(num2);

	let num3 = num1 + num2;

//solving 2 numbers
	if (num3 < 10){
		num3 = num1 + num2;
	}

	else if (num3 >= 10 && num3 <= 19){
		num3 = num2 - num1;
	}

	else if (num3 >= 20 && num3 <= 29){
		num3 = num1 * num2;
	}

	else if (num3 >= 30) {
		num3 = num1 / num2;
	}

	else{
		console.log(`Invalid input`);
	}

//displaying solved numbers
	if (num3 >= 10){
		alert(`Value is ${num3}`);
	}
	else if (num3 <= 9){
		console.warn(`Value is ${num3}`);
	}
}

solveNumbers();

function legalAge(){
	let name = prompt(`Enter your name`);
	let age = parseInt(prompt(`Enter your age`));

//check name and age
	if (!name || !age){
		console.log(`Are you a time traveler?`);
	}
	else{
		console.log(`Name: ${name.toUpperCase()}, Age: ${age}`)

		//validate age of user
		if(age >= 18){
			alert(`You are of legal age`);

			switch(age){
				case 18:
				console.log(`You are now allowed to party`);
				break;
				case 21:
				console.log(`You are now part of the adult society`);
				break;
				case 65:
				console.log(`We thank you for your contribution to society`);
				break;
				default:
				console.log(`Are you sure you're not an alien?`);
				break;
			}
		}
		else{
			alert(`You are not allowed here`);
		}
	}
}

legalAge();



function forceErr(){
	try{
		alerat(`This is a message`);
	}

	catch(error){
		console.warn(error.message);
	}

	finally{
		alert(`Another message`);
	}
}

forceErr();